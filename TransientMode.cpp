#include <Arduino.h>
#include <LiquidCrystal_I2C.h>
#include "TransientMode.h"
#include "DclTools.h"
#include "PowerMode.h"
#include "UserData.h"
#include "TransientController.h"
#include "TransientLoopController.h"
#include "TransientToggleController.h"
#include "TransientPulseController.h"
#include "TransientListController.h"

static const char* transientType[7]{ "Loop", "Toggle", "Pulse", "List" };

TransientMode::TransientMode(
	LiquidCrystal_I2C* lcd,
	DacControl* dacControl,
	TemperatureControl* temperatureControl,
	CurrentVoltageControl* currentVoltageControl,
	UserData* userData) : PowerMode(lcd, dacControl, temperatureControl, currentVoltageControl, userData)
{}

TransientMode::~TransientMode()
{
	if (_transientController) delete _transientController;
	_transientController = NULL;
}

DclMode TransientMode::GetMode() { return ModeTR; }
float TransientMode::GetDacCurrent() { return _transientController ? _transientController->_currentCurrent : 0; }

void TransientMode::Initialize()
{
	DclTools::PrintList(_lcd, TransientTypeCount, transientType);
	_userInput->Setup(1, TransientTypeCount, this);
}

void TransientMode::Update(DclInputs* inputs)
{
	if (!_transientController || !_transientController->_isInitialized) printInput();
	else if (_transientController->_isInitialized && _step == 0)
	{
		_lcd->noCursor();
		_lcd->clear();
		_step = 1;
	}
	else if (_transientController->_isInitialized && _step == 1)
	{
		PowerMode::Update(inputs);
		if (_transientController->Update(inputs) && _dacControl->isOn()) SwitchLoad(true);
	}
}

void TransientMode::inputCallback(UserInputState state, byte step)
{
	if (state != UserInputConfirmed) return;
	if (!_transientController) ControllerFactory(_userInput->ToInt()-1);
}

void TransientMode::ControllerFactory(int selectedType)
{
	_selectedType = selectedType;
	switch (_selectedType)
	{
		case 1: _transientController = new TransientToggleController(_lcd, _userData, _userInput); break;
		case 2: _transientController = new TransientPulseController(_lcd, _userData, _userInput); break;
		case 3: _transientController = new TransientListController(_lcd, _userData, _userInput); break;
		default: _transientController = new TransientLoopController(_lcd, _userData, _userInput); break;
	}
	_transientController->Initialize();
}

void TransientMode::LoadSwitched(bool on)
{
	if (_transientController && _transientController->_isInitialized) _transientController->SwitchLoad(on);
	PowerMode::LoadSwitched(on);
}

bool TransientMode::UpdateUI()
{
	PowerMode::UpdateUI();

	_lcd->setCursor(ModeExtraColumn, StatusRow);
	_lcd->print(transientType[_selectedType]);

	if (_transientController && _transientController->_isInitialized) _transientController->UpdateUI();

	return false;
}

