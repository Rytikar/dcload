#include <LiquidCrystal_I2C.h>
#include "TransientListController.h"
#include "TransientController.h"
#include "UserData.h"
#include "DclConst.h"
#include "UserInput.h"

TransientListController::TransientListController(LiquidCrystal_I2C* lcd, UserData* userData, UserInput* userInput) : TransientController(lcd, userData, userInput)
{}

TransientListController::~TransientListController()
{
	if (_listItems) delete _listItems;
	_listItems = NULL;
}

void TransientListController::Initialize()
{
	_lcd->clear();
	_lcd->print(F("List size (2-10):"));
	_step = _userInput->Setup(2, 2, 10, 2, 0, this, 17, 0, NulChar, 0);
}

bool TransientListController::Update(DclInputs* inputs)
{
	if (!_isInitialized) return false;

	if (_step == 3 && _isOn)
	{
		long newMillis = millis();
		_leftMillis = _nextMillis - newMillis;
		if (_leftMillis > 0) return false;

		_currentItem = _currentItem + 1 < _itemCount ? _currentItem + 1 : 0;
		_nextItem = _currentItem + 1 < _itemCount ? _currentItem + 1 : 0;
		_currentCurrent = _listItems[_currentItem].current;
		_nextCurrent = _listItems[_nextItem].current;
		_nextMillis = newMillis + _listItems[_currentItem].time;
		return true;
	}

	return false;
}

void TransientListController::SwitchLoad(bool on)
{
	if (on && _isOn) return;
	if (_isOn = on)
	{
		_currentItem = -1;
		_nextMillis = 0;
	}
}

void TransientListController::inputCallback(UserInputState state, byte step)
{
	if (state != UserInputConfirmed) return;
	if (step == 0)
	{
		_itemCount = _userInput->ToInt();
		_listItems = new TransientListItem[_itemCount];
		_currentItem = 0;
		setupItem(_currentItem);
	}
	else if (step == 1)
	{
		_listItems[_currentItem].current = _userInput->ToFloat();
		_step = _userInput->Setup(0, 0, 999.9, 5, 1, this, UserInputColumn, 3, NulChar, 2);
	}
	else if (step == 2)
	{
		_listItems[_currentItem].time = getUserTime();
		if (_currentItem == _itemCount - 1) finishSetup();
		else setupItem(++_currentItem);
	}
}

void TransientListController::finishSetup()
{
	_currentCurrent = _listItems[_currentItem = 0].current;
	_nextCurrent = _listItems[_nextItem = 1].current;
	_isInitialized = _step = 3;
}

void TransientListController::setupItem(byte item)
{
	_lcd->setCursor(0, 1);
	_lcd->print(F("Item "));
	_lcd->print(item+1);
	_lcd->print("/");
	_lcd->print(_itemCount);
	_lcd->setCursor(0, 2);
	_lcd->print(F("I  ="));
	_lcd->setCursor(0, 3);
	_lcd->print(F("Sec="));
	_step = _userInput->Setup(0, 0, _userData->getValue(CurrentCutOff), UserInputLength, 3, this, UserInputColumn, 2, CurrentUnit, 1);
}

void TransientListController::UpdateUI()
{
	PrintValues(&_currentCurrent, &_nextCurrent, &_listItems[_currentItem].time);
	if (_isOn) PrintIn(_leftMillis);
}
