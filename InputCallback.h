// InputCallback.h

#ifndef _INPUTCALLBACK_h
#define _INPUTCALLBACK_h

#if defined(ARDUINO) && ARDUINO >= 100
#include <arduino.h>
#else
#include "WProgram.h"
#endif

#include "DclConst.h"

class InputCallback
{
public:
	virtual void inputCallback(UserInputState state, byte step) = 0;
};

#endif

