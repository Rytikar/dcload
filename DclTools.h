#ifndef DclTools_H
#define DclTools_H
 
#include <Arduino.h>
#include <LiquidCrystal_I2C.h>

class DclTools 
{
  public:
    static bool IsIn(int value, int minValue, int maxValue);
	static bool IsInput(DclInputs* inputs);
	static char* CreateString(int strLength, char c);
    static void LcdClearRows(LiquidCrystal_I2C* lcd, byte fromRow, byte toRow);
	static void PrintList(LiquidCrystal_I2C* lcd, byte count, const char* texts[]);
	static void PrintSet(LiquidCrystal_I2C* lcd, byte column, byte row, char quantity);
	static void PrintValue(LiquidCrystal_I2C* lcd, byte column, byte row, float value, int width, int precision, char unit);
	static char* floatToStr(float value, int width, int precision, char* result, char filler = SpaceChar);
	static float str2Float(const char * str);
	static long addSample(const long newSample, const long oldSample, int* sampleCnt, const int delta, const int reject = 0);
};
 
#endif
