// DacControl.h

#ifndef _DACCONTROL_h
#define _DACCONTROL_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include <arduino.h>
#else
	#include "WProgram.h"
#endif
#include <Adafruit_MCP4725.h> 

class DacControl
{
public:
	DacControl(Adafruit_MCP4725* dac);
	void init();
	void setCurrent(float current, bool writeEeprom = false);
	float getCurrent();
	void cutOff();
	void resetCutOff();
	bool isCutOff();
	bool isOn();
private:
	Adafruit_MCP4725* _dac;
	float _lastCurrent = 0;
	bool _isCutOff = false;
};


#endif

