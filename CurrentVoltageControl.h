#ifndef CurrentVoltageControl_H
#define CurrentVoltageControl_H

#include <Arduino.h>
#include "DclConst.h"
#include "MCP3426.h"

class CurrentVoltageControl
{
public:
	CurrentVoltageControl();
	~CurrentVoltageControl();
	void init();
	CurrentVoltage getCurrentVoltage();
private:
	MCP3426* _adc;
	long _voltSample;
	long _currentSample;
	int _voltSampleCnt;
	int _currentSampleCnt;
};

#endif
