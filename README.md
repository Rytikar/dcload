# Introduction #

The DcLoad repo is a reimplementation of the Arduino sofware for the [Scullcom DC Load](https://www.youtube.com/watch?v=9fsf1CgnTRk&list=PLUMG8JNssPPzbr4LydbTcBrhoPlemu5Dt&index=1) project.

It uses the 4x4 keypad, as that's what I've got. I ordered a 4x5 and will add functionality for it, but it takes some time to be delivered.

* v1.0, 26 May 2018
    * first release
* v1.0.01, 4. June 2018
    * fixed bug me soldering the rotary connections the wrong way. Up and down switched
* v1.1, 8. September 2018
    * fixed bug in rotary encoder irq conflicting with dac
    * fixed bug in 100ms/1s timing processing leading to hang unit
    * refactored lcd display update timing and speed to make it more smooth
    * refactored user input via keypad and rotary encoder
    * refactored cutoff ui and handling
    * refactored fan handling. It runs at least one minute when turned on and turns off when temperature is 2 degrees under start temperature.
    * removed serial output, as that will redone in v2.0 as a remote controllable unit with windows software
    * trimmed 4k of memory and 200bytes of ram usage in preparation for v2.0
* v1.2, 23. September 2018
    * fixed bug not initializing pns correctly
    * fixed bug with user battery voltage one digit only. Now two, up to 30V
    * fixed bug power mode input two digits only. Now three, up to 150W
    * added feature. Rotary button now moves through the whole number
* v1.3, 25. October 2018
    * fixed bug on first startup with uninitialized EEPROM values. Thanks to Georg.
    * removed MCP3426 ADC library and implemented my own. Using 'continuous mode' now instead of 'one shot'.
Much faster update rate, 'one shot' took close to 200ms each time reading voltage and current. Freed up 2k, too.
    * sampling voltage and current now with 'exponential moving average' calculation resulting in stable display for all digits

Please report errors if you find some!

This readme and documentation is under construction...

This looks to me as a quite large project for the Arduino Nano. As Louis am I struggling with memory space. With that said, I think that it will be easier to add features (to the already quite feature rich DC Load) with the (IMHO) more structured software approach.
I've not implemented much more to the serial interface as there had been in the original. Still, the idea is to make room for a remote controlled DC Load with accompanying PC software to fully remote control the DC Load. We will see.

It will be of interest what OS people are using. Obviously mainly Windows, but how many Linux or Mac users are out there? That will have an influence of how to develop the PC part of the remote control.

### Installation ###

* Click on the [Downloads](https://bitbucket.org/Rytikar/dcload/downloads/) link.
* Click on 'Download repository'
* You will receive a .zip file, containing the repository
* Unzip the file into a folder called 'dcload'. That is important, as the Arduino IDE needs it to work
* You will end up with lots of files. That's because I have implemented the software with classes, splitting the source in several files. But don't worry...
* Open the dcload.ino file in the Arduino IDE as usual. You will see the IDE opens ALL .h and .cpp files, too. That makes handling the project a bit unwieldy and was the reason I decided to move to Visual Studio. That's where the .sln and other VS specific files come from.
* Build and upload the proojec to your Arduino as usual. 
* Have fun

### A bit of history ###

First a big thank you to Louis for providing this exiting project. It was a pleasure listening to the videos, learning and building.

I started looking into the code with the idea of changing some minor things. For example am I using a bigger heatsink I had and wanted to set the fan start temperature higher.
In the end, my software engineering habits took hold of me, and I decide to rewrite the whole software. Why? Because I could!
The main reason was of course learning how to program the Arduino, how to do it properly in C++, and figure out in detail how the DC Load hardware is handled in the software.

The result is in this repository.

### Documentation ###

The look and feel resembles the original software. There's only so much you can do on a 20x4 display.
Still, I've changed the key and rotary usage a bit.

* Implements all of the features available in Louis original software. Some enhancements done.
* I've implemented a 'Hold' mode (press 1 second or longer) for both keys and rotary button. 
* A-D changes modes. A:CC, B:CP, C:CR, D:Battery, Hold-A: Transient, Hold-C: Setup, Hold-D: SplashScreen
* When entering data:
	* numbers enter numbers...
	* Hash: deletes one entry
	* Hold-Hash: deletes all
	* Star: toggles between integral or fractional part of number
	* Hold-Star: confirms (Enter)
	* Rotary Button: Moves the cursor to the next digit.
	* Hold-Rotary Button: confirms (Enter)
* When not in transient modes, the current value (amps or power) can be changed and set as load by 'confirming' if load is on.

### Hardware ###

Latest hardware of the DC Load project.
I used the double sided print from Louis version 9.2, but I believe it will work with all hardware feature complete builds.

### Who do I talk to? ###

* dps@rosenkilde-hoppe.dk

### Thanks ###

To:

* [Scullcom](http://www.scullcom.uk/)
* [VisualMicro](https://www.visualmicro.com/)