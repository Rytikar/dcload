#include <LiquidCrystal_I2C.h>
#include "TransientPulseController.h"
#include "TransientController.h"
#include "UserData.h"
#include "DclConst.h"
#include "UserInput.h"

TransientPulseController::TransientPulseController(LiquidCrystal_I2C* lcd, UserData* userData, UserInput* userInput) : TransientController(lcd, userData, userInput)
{};

void TransientPulseController::Initialize()
{
	initialize(true);
}

bool TransientPulseController::Update(DclInputs* inputs)
{
	if (!_isInitialized) return false;
	if (_step == 3 && _isOn)
	{
		long newMillis = millis();
		if (digitalRead(pinTriggerPulse) == LOW)
		{
			if (_currentCurrent == _current2) return false;
			_currentCurrent = _current2;
			_nextCurrent = _current1;
			_nextMillis = newMillis + _time;
			_leftMillis = _time;
			return true;
		}
		else if (newMillis > _nextMillis)
		{ 
			if (_currentCurrent == _current1) return false;
			_currentCurrent = _current1;
			_nextCurrent = _current2;
			_nextMillis = 0;
			return true;
		}
		else if (_nextMillis != 0)
		{
			_leftMillis = _nextMillis - newMillis;
			return false;
		}
	}
	return false;
}

void TransientPulseController::SwitchLoad(bool on)
{
	if (on && _isOn) return;
	if (_isOn = on) _nextMillis = 0;
}

void TransientPulseController::inputCallback(UserInputState state, byte step)
{
	if (state != UserInputConfirmed) return;
	if (step == 0)
	{
		_current1 = _userInput->ToFloat();
		_step = _userInput->Setup(0, 0, _userData->getValue(CurrentCutOff), UserInputLength, 3, this, UserInputColumn, 1, CurrentUnit, 1);
	}
	else if (step == 1)
	{
		_current2 = _userInput->ToFloat();
		_step = _userInput->Setup(0, 0, 999.9, 5, 1, this, UserInputColumn, 2, NulChar, 2);
	}
	else if (step == 2)
	{
		_time = getUserTime();
		_currentCurrent = _current1;
		_nextCurrent = _current2;
		_isInitialized = _step = 3;
	}
}

void TransientPulseController::UpdateUI()
{
	PrintValues(&_currentCurrent, &_nextCurrent, &_time);

	if (!_isOn) return;
	char tmp[7];
	DclTools::floatToStr(_nextCurrent, -1, 3, tmp);
	if (_nextMillis == 0)
	{
		_lcd->setCursor(0, ExtraRow);
		_lcd->print(F("Pulse for "));
		_lcd->print(tmp);
		_lcd->print(CurrentUnit);
	}
	else PrintIn(_leftMillis, tmp, CurrentUnit);
}
