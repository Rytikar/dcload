#include <Arduino.h>
#include <EEPROM.h>
#include "UserData.h"
#include "DclConst.h"
#include "DclTools.h"

UserData::UserData(void)
{
	_setupValues = new byte[SetupValuesCount];
}

UserData::~UserData(void)
{
	delete(_setupValues);
}

void UserData::read(void)
{
	for (int i = 0; i < SetupValuesCount; i++)
		setValue((SetupValue)i, readMinMax(i, 0, SetupMaxValues[i], SetupMaxValues[i]));
	_userBatteryCutOff = readMinMax(UserBatteryCutOffEEAddress, 0, MaxVoltage, 0);
}

void UserData::write(void)
{
	for (int i = 0; i < SetupValuesCount; i++)
		EEPROM.write(i, _setupValues[SetupValuesEEAddress + i]);
}

bool UserData::isValid(void)
{
	for (int i = 0; i < SetupValuesCount; i++)
		if (!DclTools::IsIn(_setupValues[i], 0, SetupMaxValues[i])) return false;
	return true;
}

byte UserData::getValue(SetupValue setupValue) { return _setupValues[setupValue]; }
void UserData::setValue(SetupValue setupValue, byte value) { _setupValues[setupValue] = value; }
float UserData::getUserBatteryCutOff() { return _userBatteryCutOff; }

void UserData::writeUserBatteryCutOff(float value)
{
	_userBatteryCutOff = writeFloat(UserBatteryCutOffEEAddress, value);
}

const __FlashStringHelper* UserData::getText(SetupValue setupValue)
{
	static const __FlashStringHelper *texts[] =
	{
	  F("Current limit"),
	  F("Power limit"),
	  F("Temp cutoff"),
	  F("Temp fan on"),
	  F("Flash skip")
	};
	return texts[setupValue];
}

byte UserData::readMinMax(int idx, byte min, byte max, byte def)
{
	byte v = EEPROM.read(SetupValuesEEAddress + idx);
	return v < min ? def: (v > max ? def : v);
}

float UserData::readMinMax(int address, float min, float max, float def)
{
	float v;
	EEPROM.get(address, v);
	return (v == NAN || v < min || v > max) ? def : v;
}

float UserData::writeFloat(int address, float value)
{
	EEPROM.put(address, value);
	return value;
}

