#include "TransientController.h"
#include "UserData.h"
#include "DclConst.h"

TransientController::TransientController(LiquidCrystal_I2C* lcd, UserData* userData, UserInput* userInput)
{
	_lcd = lcd;
	_userData = userData;
	_userInput = userInput;
}

TransientController::~TransientController() {}

void TransientController::initialize(bool seconds)
{
	_lcd->clear();
	_lcd->print(F("I1 ="));
	_lcd->setCursor(0, 1);
	_lcd->print(F("I2 ="));

	if (seconds)
	{
		_lcd->setCursor(0, 2);
		_lcd->print(F("Sec="));
	}
	_step = _userInput->Setup(0, 0, _userData->getValue(CurrentCutOff), UserInputLength, 3, this, UserInputColumn, 0, CurrentUnit, 0);
}

long TransientController::getUserTime()
{
	long time = _userInput->ToFloat() * 1000;
	return time <= 100 ? 100 : time;
}

void TransientController::PrintIn(long leftMillis, char* text1 = "Next", char text2 = NulChar)
{
	_lcd->setCursor(0, ExtraRow);
	_lcd->print(text1);
	if (text2) _lcd->print(text2);
	_lcd->print(F(" in: "));
	_lcd->print(leftMillis);
	_lcd->print(F("ms    "));
}

void TransientController::PrintValues(float* current, float* nextCurrent, unsigned long* time = NULL)
{
	DclTools::PrintValue(_lcd, 0, 2, *current, 5, 3, CurrentUnit);
	DclTools::PrintValue(_lcd, 7, 2, *nextCurrent, 5, 3, CurrentUnit);
	if (time) DclTools::PrintValue(_lcd, 14, 2, *time / 1000.0, 5, 1, SecondUnit);
}

void TransientController::UpdateUI()
{}
