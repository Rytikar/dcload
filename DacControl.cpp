#include "DacControl.h"

DacControl::DacControl(Adafruit_MCP4725* dac)
{
	_dac = dac;
}

void DacControl::init()
{
	_dac->begin(0x61);                                       //the DAC I2C address with MCP4725 pin A0 set high
	setCurrent(0, true);
}

void DacControl::setCurrent(float current, bool writeEeprom = false)
{
	if (_isCutOff || (_lastCurrent == current && !writeEeprom)) return;
	unsigned long dacValue = current * 1000.0;
	_dac->setVoltage(dacValue, writeEeprom);
	_lastCurrent = current;
}

float DacControl::getCurrent()
{
	return _lastCurrent;
}

void DacControl::cutOff()
{
	setCurrent(0);
	_isCutOff = true;
}

void DacControl::resetCutOff()
{
	_isCutOff = false;
}

bool DacControl::isCutOff()
{
	return _isCutOff;
}

bool DacControl::isOn()
{
	return _lastCurrent != 0;
}