#include <LiquidCrystal_I2C.h>
#include "TransientLoopController.h"
#include "TransientController.h"
#include "TransientListController.h"
#include "UserData.h"
#include "DclConst.h"
#include "UserInput.h"

TransientLoopController::TransientLoopController(LiquidCrystal_I2C* lcd, UserData* userData, UserInput* userInput) : TransientListController(lcd, userData, userInput)
{};

void TransientLoopController::Initialize()
{
	_itemCount = 2;
	_listItems = new TransientListItem[_itemCount];
	initialize(true);
}

void TransientLoopController::inputCallback(UserInputState state, byte step)
{
	if (state != UserInputConfirmed) return;
	if (step == 0)
	{
		_listItems[0].current = _userInput->ToFloat();
		_step = _userInput->Setup(0, 0, _userData->getValue(CurrentCutOff), UserInputLength, 3, this, UserInputColumn, 1, CurrentUnit, 1);
	}
	else if (step == 1)
	{
		_listItems[1].current = _userInput->ToFloat();
		_step = _userInput->Setup(0, 0, 999.9, 5, 1, this, UserInputColumn, 2, NulChar, 2);
	}
	else if (step == 2)
	{
		_listItems[0].time = getUserTime();
		_listItems[1].time = _listItems[0].time;
		finishSetup();
	}
}
