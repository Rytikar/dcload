#include <Arduino.h>
#include <LiquidCrystal_I2C.h>
#include "BatteryCapacityMode.h"
#include "DclConst.h"
#include "DclTools.h"
#include "PowerMode.h"
#include "TemperatureControl.h"

PowerMode::PowerMode(
	LiquidCrystal_I2C* lcd,
	DacControl* dacControl,
	TemperatureControl* temperatureControl,
	CurrentVoltageControl* currentVoltageControl,
	UserData* userData) : BaseMode(lcd, userData)
{
	_dacControl = dacControl;
	_temperatureControl = temperatureControl;
	_currentVoltageControl = currentVoltageControl;
	_userInput = new UserInput();
	SwitchLoad(false);
}

PowerMode::~PowerMode()
{
	SwitchLoad(false);
}

void PowerMode::Update(DclInputs* inputs)
{
	_currentVoltage = _currentVoltageControl->getCurrentVoltage();
	_currentTemperature = _temperatureControl->getCurrentTemperature();

	if (inputs->LoadButton_State != LoadButtonIdle) SwitchLoad(!_dacControl->isOn());

	BaseMode::Update(inputs);
}

void PowerMode::SwitchLoad(bool on)
{
	if (_dacControl->isCutOff()) _dacControl->resetCutOff();
	else
	{
		_dacControl->setCurrent(on ? GetDacCurrent() : 0);
		LoadSwitched(_dacControl->isOn());
	}
}

void PowerMode::LoadSwitched(bool on)
{}

//---------------------

bool PowerMode::UpdateUI()
{
	BaseMode::UpdateUI();

	PrintLoadStatus();
	PrintTemperature();
	PrintCurrentVoltage();

	return true;
}

void PowerMode::PrintLoadStatus()
{
	_lcd->setCursor(LoadStatusColumn, StatusRow);
	if (_dacControl->isOn() || _dacControl->isCutOff()) _lcd->print(_1sToggle ? F("     ") : (_dacControl->isCutOff() ? F("<CUT>") : F("<ON> ")));
	else _lcd->print(F("<OFF>"));
}

void PowerMode::PrintTemperature()
{
	_lcd->setCursor(TemperatureColumn, StatusRow);
	_lcd->print(_currentTemperature.temperatureState == TemperatureStateFanOn ? FanOnChar : SpaceChar);
	_lcd->print(_currentTemperature.temperature);
	_lcd->print(TemperatureUnit);
}

void PowerMode::PrintCurrentVoltage()
{
	float power = _currentVoltage.voltage * _currentVoltage.current;
	byte powerDecimals = power < 10 ? 2 : (power < 100 ? 1 : 0);

	DclTools::PrintValue(_lcd, CurrentVoltageColumn, ActualRow, _currentVoltage.current, 5, 3, CurrentUnit);
	DclTools::PrintValue(_lcd, CurrentVoltageColumn + 7, ActualRow, _currentVoltage.voltage, 6, 3, VoltageUnit);
	DclTools::PrintValue(_lcd, CurrentVoltageColumn + 15, ActualRow, power, 4, powerDecimals, PowerUnit);
}
