#ifndef SetupMode_H
#define SetupMode_H

#include <Arduino.h>
#include <LiquidCrystal_I2C.h>
#include <Keypad.h>
#include "BaseMode.h"
#include "PowerMode.h"
#include "UserData.h"

struct SetupPosition
{
	byte Page;
	byte Row;
	bool PageSkip;
	bool LastItem;

	SetupPosition::SetupPosition(byte page, byte row, bool pageSkip, bool lastItem)
	{
		Page = page;
		Row = row;
		PageSkip = pageSkip;
		LastItem = lastItem;
	}
};

class SetupMode : public BaseMode
{
public:
	SetupMode(LiquidCrystal_I2C* lcd, UserData* userData);
	void Initialize() override;
	bool UpdateUI() override;
	void inputCallback(UserInputState state, byte step) override;
	static void PrintCurrentSetup(LiquidCrystal_I2C* lcd, UserData* userData);
	DclMode GetMode();
private:
	static const byte ValueColumn = 14;
	static const byte ValueRows = 3;
	static const byte ValueFirstRow = 1;

	byte _step = 0;
	static void PrintSetup(LiquidCrystal_I2C* lcd, byte row, byte item, int value);
	static void PrintSetupHeader(LiquidCrystal_I2C* lcd, const char * text, int page);
	static SetupPosition GetSetupPosition(byte item, byte itemCount);
	void Reset();
	void InitializeStep();
	void NextStep();
};

#endif
