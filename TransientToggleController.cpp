#include <LiquidCrystal_I2C.h>
#include "TransientToggleController.h"
#include "TransientController.h"
#include "UserData.h"
#include "DclConst.h"
#include "UserInput.h"

TransientToggleController::TransientToggleController(LiquidCrystal_I2C* lcd, UserData* userData, UserInput* userInput) : TransientController(lcd, userData, userInput)
{};

void TransientToggleController::Initialize()
{
	initialize(false);
}

bool TransientToggleController::Update(DclInputs* inputs)
{
	if (!_isInitialized) return false;
	if (_step == 2 && _isOn)
	{
		if (inputs->TriggerPulse_State != TriggerLow) return false;
		_currentCurrent = _currentCurrent == _current1 ? _current2 : _current1;
		_nextCurrent = _currentCurrent == _current1 ? _current2 : _current1;
		return true;
	}
	return false;
}

void TransientToggleController::SwitchLoad(bool on)
{
	if (on && _isOn) return;
	if (_isOn = on)
	{
		_currentCurrent = _current1;
		_nextCurrent = _current2;
	}
}

void TransientToggleController::inputCallback(UserInputState state, byte step)
{
	if (state != UserInputConfirmed) return;
	if (step == 0)
	{
		_current1 = _userInput->ToFloat();
		_step = _userInput->Setup(0, 0, _userData->getValue(CurrentCutOff), UserInputLength, 3, this, UserInputColumn, 1, CurrentUnit, 1);
	}
	else if (step == 1)
	{
		_current2 = _userInput->ToFloat();
		_currentCurrent = _current1;
		_nextCurrent = _current2;
		_isInitialized = _step = 2;
	}
}

void TransientToggleController::UpdateUI()
{
	PrintValues(&_currentCurrent, &_nextCurrent);
}
