#include "MCP3426.h"
#include <Wire.h>

void MCP3426::init()
{
	_i2cAddress = B1101000;	// Hardcoded for MCP3426
	VRef = 2.048;

	Wire.beginTransmission(_i2cAddress);
	Wire.write(CONFIG_C0);
	Wire.endTransmission();

	Wire.beginTransmission(_i2cAddress);
	Wire.write(CONFIG_C1);
	Wire.endTransmission();

	_currentChannel = CHANNEL_1;
}

int MCP3426::readADC(long* value)
{
	Wire.requestFrom(_i2cAddress, (byte)3);
	int hi = Wire.read();
	int lo = Wire.read();
	bool done = (Wire.read() & B10000000) == 0;
	long v = hi << 8 | lo;
	if (v >= 32768) v = 65536l - v;
	*value = v < 0 ? 0 : v;
	int result = done ? _currentChannel : -1;

	if (done)	// Conversion done for current channel, switch channel
	{
		_currentChannel = _currentChannel == CHANNEL_0 ? CHANNEL_1 : CHANNEL_0;
		Wire.beginTransmission(_i2cAddress);
		Wire.write(_currentChannel == CHANNEL_0 ? SELECT_C0 : SELECT_C1);
		Wire.endTransmission();
	}

	return result;
}
