#ifndef PowerMode_H
#define PowerMode_H
 
#include <Arduino.h>
#include <LiquidCrystal_I2C.h>
#include "BaseMode.h"
#include "UserData.h"
#include "DacControl.h"
#include "TemperatureControl.h"
#include "CurrentVoltageControl.h"

class PowerMode : public BaseMode
{
  public:
    PowerMode(
      LiquidCrystal_I2C* lcd, 
	  DacControl* dacControl,
      TemperatureControl* temperatureControl, 
      CurrentVoltageControl* currentVoltageControl, 
      UserData* userData);
    virtual ~PowerMode();
    void Update(DclInputs* inputs) override;
	bool UpdateUI() override;
  protected:  
    CurrentVoltageControl* _currentVoltageControl;
	DacControl* _dacControl;
	TemperatureControl* _temperatureControl;
	CurrentVoltage _currentVoltage = CurrentVoltage(0, 0);
	CurrentTemperature _currentTemperature = CurrentTemperature(0, TemperatureStateFanOff);
	virtual float GetDacCurrent() = 0;
	void SwitchLoad(bool on);
	virtual void LoadSwitched(bool on);
private:
	void PrintLoadStatus();
	void PrintTemperature();
	void PrintCurrentVoltage();
};
 
#endif
