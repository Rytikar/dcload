#include <Arduino.h>
#include <LiquidCrystal_I2C.h>
#include "FlashScreenMode.h"
#include "BaseMode.h"

FlashScreenMode::FlashScreenMode(LiquidCrystal_I2C* lcd, UserData* userData) : BaseMode(lcd, userData)
{}

void FlashScreenMode::Initialize()
{
	_lcd->noCursor();
	PrintFlashScreen(_lcd, false);
}

bool FlashScreenMode::UpdateUI()
{
	return true;
}

void FlashScreenMode::inputCallback(UserInputState state, byte step)
{}

DclMode FlashScreenMode::GetMode() { return ModeFlash; }

void FlashScreenMode::PrintFlashScreen(LiquidCrystal_I2C* lcd, bool doDelay)
{
	lcd->clear();
	lcd->setCursor(6, 0);
	lcd->print(F("SCULLCOM"));
	lcd->setCursor(1, 1);
	lcd->print(F("Hobby Electronics"));
	lcd->setCursor(1, 2);
	lcd->print(F("DC Electronic Load"));
	lcd->setCursor(0, 3);
	lcd->print(F("V1.3rrh"));
	lcd->setCursor(11, 3);
	lcd->print(F("Mem:"));
	lcd->print(FreeRam());
	if (doDelay) delay(ConvenienceDelay);
}

int FlashScreenMode::FreeRam()
{
	extern int __heap_start, *__brkval;
	int v;
	return (int)&v - (__brkval == 0 ? (int)&__heap_start : (int)__brkval);
}

