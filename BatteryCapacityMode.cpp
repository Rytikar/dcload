#include <Arduino.h>
#include <LiquidCrystal_I2C.h>
#include <MCP79410_Timer.h>
#include <math.h>
#include "BatteryCapacityMode.h"
#include "DclConst.h"
#include "DclTools.h"
#include "PowerMode.h"
#include "ManualPowerMode.h"
#include "UserData.h"

static const char* batteryType[6]{ "LiPo", "FiFe", "NiCd", "ZiZn", "PbAc", "User" };
static const float batteryCutOff[5]{ 3.0, 2.8, 1.0, 1.0, 1.75 };

BatteryCapacityMode::BatteryCapacityMode(
	LiquidCrystal_I2C* lcd,
	DacControl* dacControl,
	TemperatureControl* temperatureControl,
	CurrentVoltageControl* currentVoltageControl,
	UserData* userData,
	MCP79410_Timer* timer) : ManualPowerMode(lcd, dacControl, temperatureControl, currentVoltageControl, userData)
{
	_timer = timer;
	_timer->reset();
}

void BatteryCapacityMode::Update(DclInputs* inputs)
{
	if (_step < 2) printInput();
	else
	{
		PowerMode::Update(inputs);
		if (_dacControl->isOn() && _currentVoltage.voltage < _selectedCutOff) SwitchLoad(false);
		HandleTimer(&_currentVoltage);
	}
}

void BatteryCapacityMode::Initialize()
{
	DclTools::PrintList(_lcd, BatteryTypeCount, batteryType);
	_step = _userInput->Setup(1, BatteryTypeCount, this, 0);
}

void BatteryCapacityMode::inputCallback(UserInputState state, byte step)
{
	if (state != UserInputConfirmed) return;
	switch (step)
	{
	case 0: HandleType(_userInput->ToInt() - 1); break;
	case 1: FinalSetup(_userInput->ToFloat()); break;
	default: if (_dacControl->isOn()) SwitchLoad(true); break;
	}
}

DclMode BatteryCapacityMode::GetMode() { return ModeBC; }
char BatteryCapacityMode::Quantity() { return CurrentQuantity; }
void BatteryCapacityMode::UserInputSetup() { _step = _userInput->Setup(0, 0, _userData->getValue(CurrentCutOff), UserInputLength, 3, this, UserInputColumn, ModeRow, CurrentUnit, 2); }

float BatteryCapacityMode::GetDacCurrent()
{
	return _userInput->ToFloat();
}

void BatteryCapacityMode::HandleType(int selectedType)
{
	_selectedType = selectedType;
	if (_selectedType < BatteryTypeCount - 1) FinalSetup(batteryCutOff[_selectedType]);
	else
	{
		DclTools::PrintSet(_lcd, 0, ExtraRow, VoltageQuantity);
		_step = _userInput->Setup(_userData->getUserBatteryCutOff(), 0, MaxVoltage, UserInputLength, 2, this, UserInputColumn, ExtraRow, VoltageUnit, 1);
	}
}

void BatteryCapacityMode::FinalSetup(float selectedCutOff)
{
	_selectedCutOff = selectedCutOff;
	if (_selectedType == BatteryTypeCount - 1) _userData->writeUserBatteryCutOff(_selectedCutOff);
	ManualPowerMode::Initialize();
	_step = 2;
}

void BatteryCapacityMode::HandleTimer(CurrentVoltage* currentVoltage)
{
	unsigned long lifeMillis = millis();
	if (lifeMillis < _batteryLifeMillis) return;

	if (_dacControl->isOn())
	{
		unsigned long mseconds = lifeMillis - _batteryLifeMillis + BatteryLifetimeSteps;
		_batteryLifeMillis = lifeMillis + BatteryLifetimeSteps;
		_batteryLifeAccumulated += currentVoltage->current * (mseconds / 3600.0);
	}
}

void BatteryCapacityMode::LoadSwitched(bool on)
{
	PowerMode::LoadSwitched(on);
	if (on) _batteryLifeMillis = millis() + BatteryLifetimeSteps;
	on ? _timer->start() : _timer->stop();
}

bool BatteryCapacityMode::UpdateUI()
{
	ManualPowerMode::UpdateUI();
	_lcd->setCursor(ModeExtraColumn, StatusRow);
	_lcd->print(batteryType[_selectedType]);
	DclTools::PrintValue(_lcd, BatteryCutOffColumn, ModeRow, _selectedCutOff, 4, _selectedCutOff < 10 ? 2 : 1, VoltageUnit);

	if (_dacControl->isOn())
	{
		_lcd->setCursor(0, ExtraRow);
		_lcd->print(_timer->getTime());
		_lcd->print(SpaceChar);
		_lcd->print(round(_batteryLifeAccumulated));
		_lcd->print("mAh");
	}

	return true;
}
