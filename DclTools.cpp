#include <Arduino.h>
#include <LiquidCrystal_I2C.h>
#include "DclConst.h"
#include "DclTools.h"

bool DclTools::IsIn(int value, int minValue, int maxValue)
{
	return (value >= minValue) && (value <= maxValue);
}

bool DclTools::IsInput(DclInputs* inputs)
{
	return (inputs->KeyPad_State != IDLE || inputs->Rotary_State != RotaryIdle || inputs->RotaryButton_State != RotaryButtonIdle);
}

char* DclTools::CreateString(int strLength, char c)
{
	char* str = new char[strLength + 1];
	for (int i = 0; i < strLength; i++) str[i] = c;
	str[strLength] = NulChar;
	return str;
}

void DclTools::LcdClearRows(LiquidCrystal_I2C* lcd, byte fromRow, byte toRow)
{
	for (int i = fromRow; i <= toRow; i++)
	{
		lcd->setCursor(0, i);
		char* str = DclTools::CreateString(LcdColumns, SpaceChar);
		lcd->print(str);
		delete str;
		str = NULL;
	}
}

void DclTools::PrintList(LiquidCrystal_I2C* lcd, byte count, const char* texts[])
{
	lcd->clear();
	for (int i = 0; i < count; i++)
	{
		lcd->setCursor((i % 2) == 0 ? 0 : 10, i / 2);
		lcd->print(i + 1);
		lcd->print("=");
		lcd->print(texts[i]);
	}
}

void DclTools::PrintSet(LiquidCrystal_I2C* lcd, byte column, byte row, char quantity)
{
	lcd->setCursor(column, row);
	lcd->print("Set ");
	lcd->print(quantity);
	lcd->print('=');
}

void DclTools::PrintValue(LiquidCrystal_I2C* lcd, byte column, byte row, float value, int width, int precision, char unit)
{
	char s[10];
	if (width >= 10) return;
	lcd->setCursor(column, row);
	lcd->print(DclTools::floatToStr(value, width, precision, s));
	if (unit) lcd->print(unit);
}


char* DclTools::floatToStr(float value, int width, int precision, char* result, char filler = SpaceChar)
{
	char str[14];               // Array to contain decimal string
	uint8_t ptr = 0;            // Initialise pointer for array
	uint8_t i = 0;
	int8_t digits = 1;         // Count the digits to avoid array overflow
	float rounding = 0.5;       // Round up down delta

	if (precision > 7) precision = 7;         // Limit the size of decimal portion

	for (uint8_t i = 0; i < precision; ++i) rounding /= 10.0; // Adjust the rounding value

	if (value < -rounding)    // add sign, avoid adding - sign to 0.0!
	{
		str[ptr++] = '-'; // Negative number
		str[ptr] = 0; // Put a null in the array as a precaution
		digits = 0;   // Set digits to 0 to compensate so pointer value can be used later
		value = -value; // Make positive
	}

	value += rounding; // Round up or down

	if (value >= 2147483647) return NULL;

	// No chance of overflow from here on

	// Get integer part
	unsigned long temp = (unsigned long)value;

	// Put integer part into array
	ltoa(temp, str + ptr, 10);

	if (precision > 0)
	{
		// Find out where the null is to get the digit count loaded
		while ((uint8_t)str[ptr] != 0) ptr++; // Move the pointer along
		digits += ptr;                  // Count the digits

		str[ptr++] = '.'; // Add decimal point
		str[ptr] = '0';   // Add a dummy zero
		str[ptr + 1] = 0; // Add a null but don't increment pointer so it can be overwritten

		value -= temp; // Get the decimal portion

					   // Get decimal digits one by one and put in array
					   // Limit digit count so we don't get a false sense of resolution
		while ((i < precision) && (digits < 9)) // while (i < precision) for no limit but array size must be increased
		{
			i++;
			value *= 10;       // for the next decimal
			temp = value;      // get the decimal
			ltoa(temp, str + ptr, 10);
			ptr++; digits++;         // Increment pointer and digits count
			value -= temp;     // Remove that digit
		}
	}
	if (width < 0) width = strlen(str);
	for (i = 0; i < width - strlen(str); i++) result[i] = filler;
	result[i] = 0;

	return strcat(result, str);
}

float DclTools::str2Float(const char * str)
{
	unsigned char abc;
	float ret = 0, fac = 1;
	for (abc = 9; abc & 1; str++)
	{
		abc = *str == '-' ?
			(abc & 6 ? abc & 14 : (abc & 47) | 36)
			: *str == '+' ?
			(abc & 6 ? abc & 14 : (abc & 15) | 4)
			: *str > 47 && *str < 58 ?
			abc | 18
			: (abc & 8) && *str == '.' ?
			(abc & 39) | 2
			: !(abc & 2) && (*str == ' ' || *str == '\t') ?
			(abc & 47) | 1
			:
			abc & 46;
		if (abc & 16) ret = abc & 8 ? *str - 48 + ret * 10 : (*str - 48) / (fac *= 10) + ret;
	}
	return abc & 32 ? -ret : ret;
}

long DclTools::addSample(const long newSample, const long oldSample, int* sampleCnt, const int delta, const int reject = 0)
{
	long a = abs(newSample - oldSample);
	if (reject && *sampleCnt && (a > reject)) return oldSample;
	else if ((*sampleCnt == 0) || (a > delta))
	{
		*sampleCnt = 1;
		return newSample;
	}
	else
	{
		long tmp = ((oldSample * *sampleCnt) + newSample) / (*sampleCnt + 1);
		*sampleCnt = *sampleCnt > 999 ? 100 : (*sampleCnt) + 1;
		return tmp;
	}
}
