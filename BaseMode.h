#ifndef BaseMode_H
#define BaseMode_H
 
#include <Arduino.h>
#include <LiquidCrystal_I2C.h>
#include "DclConst.h"
#include "InputCallback.h"
#include "UserData.h"
#include "UserInput.h"
 
typedef enum{ ModeNone, ModeCC, ModeCP, ModeCR, ModeBC, ModeTR, ModeSetup, ModeFlash } DclMode;
const byte ModeCount = 8;

class BaseMode : public InputCallback
{
  public:
    BaseMode(LiquidCrystal_I2C* lcd, UserData* userData);
    virtual ~BaseMode();
	virtual void Update(DclInputs* inputs);
	virtual bool UpdateUI();
	virtual void Initialize() = 0;
    virtual DclMode GetMode() = 0;
    DclMode ExitTo = ModeNone;
	UserInput* _userInput = NULL;
protected:
    const byte ModeNameColumn = 0;  
    const byte ModeExtraColumn = 3;  
    const byte LoadStatusColumn = 10;  
    const byte TemperatureColumn = 16;  
    const byte CurrentVoltageColumn = 0;  
    LiquidCrystal_I2C* _lcd;
    UserData* _userData;
	bool _100msToggle = false;
	bool _1sToggle = false;
	unsigned long _seconds = 0;
	virtual void TimerElapsed100ms();
	virtual void TimerElapsed1s();
	void printInput();
private:
	byte _current100ms = 0;
	unsigned long _currentMillis = 0;
    unsigned long _nextMillis = 0;
};
 
#endif

