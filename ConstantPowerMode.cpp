#include <Arduino.h>
#include <LiquidCrystal_I2C.h>
#include "ConstantPowerMode.h"
#include "UserData.h"
#include "ManualPowerMode.h"

ConstantPowerMode::ConstantPowerMode(
  LiquidCrystal_I2C* lcd, 
  DacControl* dacControl,
  TemperatureControl* temperatureControl, 
  CurrentVoltageControl* currentVoltageControl, 
  UserData* userData) : ManualPowerMode(lcd, dacControl, temperatureControl, currentVoltageControl, userData){}
DclMode ConstantPowerMode::GetMode(){ return ModeCP; }
char ConstantPowerMode::Quantity(){ return PowerQuantity; }
void ConstantPowerMode::UserInputSetup() { _userInput->Setup(0, 0, _userData->getValue(PowerCutOff), UserInputLength, 1, this, UserInputColumn, ModeRow, PowerUnit); }

float ConstantPowerMode::GetDacCurrent()
{
  return _currentVoltage.voltage <= 0 ? 0 : _userInput->ToFloat() / _currentVoltage.voltage;
}

