#include "ManualPowerMode.h"

ManualPowerMode::ManualPowerMode(
	LiquidCrystal_I2C* lcd,
	DacControl* dacControl,
	TemperatureControl* temperatureControl,
	CurrentVoltageControl* currentVoltageControl,
	UserData* userData) : PowerMode(lcd, dacControl, temperatureControl, currentVoltageControl, userData)
{}

void ManualPowerMode::Initialize()
{
	_lcd->clear();
	UserInputSetup();
	_lcd->clear();
}

void ManualPowerMode::inputCallback(UserInputState state, byte step)
{
	if (state == UserInputConfirmed && _dacControl->isOn()) SwitchLoad(true);
}

bool ManualPowerMode::UpdateUI()
{
	bool printInput = PowerMode::UpdateUI();

	DclTools::PrintSet(_lcd, 0, ModeRow, Quantity());

	return printInput;
}
