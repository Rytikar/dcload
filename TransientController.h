// TransientController.h

#ifndef _TRANSIENTCONTROLLER_h
#define _TRANSIENTCONTROLLER_h

#if defined(ARDUINO) && ARDUINO >= 100
#include <arduino.h>
#else
#include "WProgram.h"
#endif

#include <LiquidCrystal_I2C.h>
#include "UserData.h"
#include "UserInput.h"
#include "DclConst.h"

class TransientController : public InputCallback
{
public:
	TransientController(LiquidCrystal_I2C* lcd, UserData* userData, UserInput* userInput);
	virtual ~TransientController();
	virtual void Initialize() = 0;
	virtual bool Update(DclInputs* inputs) = 0;
	virtual void UpdateUI();
	virtual void SwitchLoad(bool on) = 0;
	bool _isInitialized = false;
	float _currentCurrent = 0;
	float _nextCurrent = 0;
protected:
	const byte UserInputLength = 7;
	const byte UserInputColumn = 4;
	byte _step = 0;
	bool _isOn = false;
	LiquidCrystal_I2C * _lcd;
	UserData* _userData;
	UserInput* _userInput;
	void initialize(bool seconds);
	long getUserTime();
	void PrintIn(long leftMillis, char* text1 = "Next", char text2 = NulChar);
	void PrintValues(float* current, float* nextCurrent, unsigned long* time = NULL);
private:
};

#endif

