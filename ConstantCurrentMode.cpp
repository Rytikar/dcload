#include <Arduino.h>
#include <LiquidCrystal_I2C.h>
#include "ConstantCurrentMode.h"
#include "UserData.h"
#include "PowerMode.h"
#include "ManualPowerMode.h"

ConstantCurrentMode::ConstantCurrentMode(
  LiquidCrystal_I2C* lcd, 
  DacControl* dacControl,
  TemperatureControl* temperatureControl, 
  CurrentVoltageControl* currentVoltageControl, 
  UserData* userData) : ManualPowerMode(lcd, dacControl, temperatureControl, currentVoltageControl, userData)
{}

DclMode ConstantCurrentMode::GetMode(){ return ModeCC; }
char ConstantCurrentMode::Quantity(){ return CurrentQuantity; }
void ConstantCurrentMode::UserInputSetup() { _userInput->Setup(0, 0, _userData->getValue(CurrentCutOff), UserInputLength, 3, this, UserInputColumn, ModeRow, CurrentUnit); }
float ConstantCurrentMode::GetDacCurrent() { return _userInput->ToFloat(); }

