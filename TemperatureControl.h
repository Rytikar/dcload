#ifndef TemperatureControl_H
#define TemperatureControl_H
 
#include <Arduino.h>
#include "DclConst.h"
#include "DacControl.h"
#include "UserData.h"

const unsigned int FANMINONMS = 60000;

class TemperatureControl
{
  public:
    TemperatureControl(DacControl* dacControl, UserData* userData);
    CurrentTemperature getCurrentTemperature();
  private:
	DacControl* _dacControl;
    UserData* _userData;
	int _sample = 0;
	int _sampleCnt = 0;
	unsigned long _onMillis = 0;
  
    bool FanCheck(Temperature temperature);
	bool CutOffCheck(Temperature temperature);
	Temperature convertedTemperature();
	int sampleTemperature();
};
 
#endif
