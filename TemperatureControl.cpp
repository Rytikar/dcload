#include <Arduino.h>
#include "TemperatureControl.h"
#include "DacControl.h"
#include "UserData.h"
#include "DclConst.h"
#include "DclTools.h"

TemperatureControl::TemperatureControl(DacControl* dacControl, UserData* userData)
{
	_dacControl = dacControl;
	_userData = userData;
}

CurrentTemperature TemperatureControl::getCurrentTemperature()
{
	Temperature temperature = convertedTemperature();
    return CurrentTemperature(temperature, CutOffCheck(temperature) ? TemperatureStateCutOff : (FanCheck(temperature) ? TemperatureStateFanOn : TemperatureStateFanOff));
}

Temperature TemperatureControl::convertedTemperature()
{
	float temp = sampleTemperature() * 0.107421875;
	return round(temp);
}

int TemperatureControl::sampleTemperature()
{
	int sample = analogRead(pinTemperature);
	sample =  analogRead(pinTemperature);
	_sample = DclTools::addSample(sample, _sample, &_sampleCnt, 10);

	return _sample;
}

bool TemperatureControl::FanCheck(Temperature temperature)
{
  byte fanStartupTemperature = _userData->getValue(FanStartupTemperature);
  
  bool isOn = digitalRead(pinFan) == HIGH;
  unsigned long m = millis();
  if (!isOn && (fanStartupTemperature == 0 || temperature >= fanStartupTemperature))
  { 
	  _onMillis = m + FANMINONMS;
	  digitalWrite(pinFan, HIGH); 
  }
  else if (isOn && fanStartupTemperature != 0)
  {
	  if (temperature >= fanStartupTemperature) _onMillis = m + FANMINONMS;
	  else if (temperature < fanStartupTemperature && _onMillis < m) digitalWrite(pinFan, LOW);
  }

  return digitalRead(pinFan) == HIGH;
}

bool TemperatureControl::CutOffCheck(Temperature temperature)
{
	byte cutOffTemperature = _userData->getValue(TemperatureCutOff);

	if (temperature < cutOffTemperature) return false;
	_dacControl->cutOff();
	return true;
}


