#include <Arduino.h>
#include "CurrentVoltageControl.h"
#include "DclConst.h"
#include "DclTools.h"
#include "MCP3426.h"

CurrentVoltageControl::CurrentVoltageControl()
{
}

CurrentVoltageControl::~CurrentVoltageControl()
{
	delete(_adc);
}

void CurrentVoltageControl::init()
{
	_adc = new MCP3426();
	_adc->init();
}

CurrentVoltage CurrentVoltageControl::getCurrentVoltage()
{
	long sample;
	int result = _adc->readADC(&sample);

	if (result == MCP3426::CHANNEL_0) _voltSample = DclTools::addSample(sample, _voltSample, &_voltSampleCnt, 5);
	else if (result == MCP3426::CHANNEL_1) _currentSample = DclTools::addSample(sample, _currentSample, &_currentSampleCnt, 5);

	return CurrentVoltage(((_currentSample * _adc->VRef) / 32768.0) * 2.5, ((_voltSample * _adc->VRef) / 32768.0) * 50.0);
}
