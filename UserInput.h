#ifndef UserInput_H
#define UserInput_H

#include <Arduino.h>
#include <LiquidCrystal_I2C.h>
#include "DclConst.h"
#include "DclTools.h"
#include "InputCallback.h"

#define MaxInputLength 10

class UserInput
{
public:
	void HandleInput(DclInputs* inputs);
	byte Setup(float value, float minValue, float maxValue, byte inputLength, byte precision, InputCallback* inputCallback, byte column = 0, byte row = 0, char unit = NulChar, byte step = 0);
	byte Setup(byte minValue, byte maxValue, InputCallback* inputCallback, byte step = 0);
	byte GetInputLength();
	int GetCursorCol();
	float ToFloat();
	int ToInt();
	long ToLong();
	const char* ToString();
	char _unit;
	byte _row;
	byte _column;
private:
	bool _inFrac;
	byte _cursor;
	float _minValue;
	float _maxValue;
	byte _inputLength;
	byte _inputIntLength;
	byte _precision;
	bool _singleKey;
	byte _step;
	InputCallback* _inputCallback;
	byte setup(float value, float minValue, float maxValue, byte inputLength, byte precision, InputCallback* inputCallback, bool singleKey, byte step);
	bool toggleInFrac(byte cursor = 0);
	bool deleteKey();
	bool clearKeys(float value);
	bool IsConfirmInput(DclInputs* inputs);
	bool HandleKey(char key, KeyState keyState);
	bool HandleSingleKey(char key, KeyState keyState);
	bool HandleRotary(RotaryState rotaryState);
	bool HandleRotaryButton(RotaryButtonState rotaryButtonState);
	bool HandleReleaseKey(char key);
	bool HandleHoldKey(char key);
	bool addKey(char key, bool advanceCursor);
	bool setKeys(const char* keys, int cursor);
	void Callback(UserInputState state);
	bool setCursor(int cursor);
};

#endif
