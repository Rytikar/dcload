#ifndef TransientMode_H
#define TransientMode_H
 
#include <Arduino.h>
#include <LiquidCrystal_I2C.h>
#include "PowerMode.h"
#include "UserData.h"
#include "TransientController.h"

class TransientMode : public PowerMode
{
  public:
    TransientMode(
      LiquidCrystal_I2C* lcd, 
	  DacControl* dacControl,
      TemperatureControl* temperatureControl, 
      CurrentVoltageControl* currentVoltageControl, 
      UserData* userData);
	~TransientMode();
	void Update(DclInputs* inputs) override;
	bool UpdateUI() override;
	void Initialize() override;
	void inputCallback(UserInputState state, byte step);
	DclMode GetMode();
  protected:
    float GetDacCurrent() override;
	void ControllerFactory(int selectedType);
    void LoadSwitched(bool on) override;
  private:
    const byte TransientTypeCount = 4;
	byte _step = 0;
	byte _selectedType;
    TransientController* _transientController = NULL;
};
 
#endif
