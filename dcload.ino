//ELECTRONIC DC LOAD PROJECT (4x4 Matrix Keypad Version)
//rrh v1.3

#include <Wire.h>                             //include I2C library
#include <LiquidCrystal_I2C.h>                // F Malpartida's NewLiquidCrystal library // https://bitbucket.org/fmalpartida/new-liquidcrystal/downloads/NewliquidCrystal_1.3.4.zip
#include <MCP79410_Timer.h>                   //Scullcom Hobby Electronics library  http://www.scullcom.com/MCP79410Timer-master.zip
#include <Keypad.h>                           //http://playground.arduino.cc/Code/Keypad
#include <EnableInterrupt.h>				          //https://github.com/GreyGnome/EnableInterrupt
//
#include "DclConst.h"
#include "DclTools.h"
#include "UserData.h"
#include "DacControl.h"
#include "TemperatureControl.h"
#include "CurrentVoltageControl.h"
#include "BaseMode.h"
#include "ConstantCurrentMode.h"
#include "ConstantPowerMode.h"
#include "ConstantResistanceMode.h"
#include "BatteryCapacityMode.h"
#include "TransientMode.h"
#include "SetupMode.h"
#include "FlashScreenMode.h"

//------------------------------------------

//define the symbols on the buttons of the keypads
char _keypadKeys[KEYPADROWS][KEYPADCOLS] = {
  {'1','2','3','A'},
  {'4','5','6','B'},
  {'7','8','9','C'},
  {'*','0','#','D'}
};

byte _keypadRowPins[KEYPADROWS] = { 5, 6, 7, 8 }; //connect to the row pin outs of the keypad
byte _keypadColPins[KEYPADCOLS] = { 9, 10, 11, 12 }; //connect to the column pin outs of the keypad

Keypad _keypad = Keypad(makeKeymap(_keypadKeys), _keypadRowPins, _keypadColPins, KEYPADROWS, KEYPADCOLS);
LiquidCrystal_I2C _lcd(0x27, 2, 1, 0, 4, 5, 6, 7);       //0x27 is the default address of the LCD with I2C bus module;
Adafruit_MCP4725 _dac;
MCP79410_Timer _timer;
//
UserData _userData;
DacControl _dacControl = DacControl(&_dac);
TemperatureControl _temperatureControl = TemperatureControl(&_dacControl, &_userData);
CurrentVoltageControl _currentVoltageControl = CurrentVoltageControl();

BaseMode* _currentMode = NULL;
volatile KeypadEvent _keypadKey;
volatile KeyState _keypadKeyState;
volatile RotaryState _rotaryState;
volatile RotaryButtonState _rotaryButtonState;
volatile LoadButtonState _loadButtonState;
volatile TriggerPulseState _triggerPulseState;

//--------------------------------Interrupt Routine for Rotary Encoder------------------------

void KeypadIsr(KeypadEvent key)
{
	static char lastHoldKey = NO_KEY;

	if (_keypadKey != NO_KEY) return;
	KeyState state = _keypad.getState();
	if (state == PRESSED || state == IDLE)
	{
		lastHoldKey = NO_KEY;
		return;
	}
	if (state == RELEASED && key == lastHoldKey) return;
	if (state == HOLD) lastHoldKey = key;

	_keypadKey = key;
	_keypadKeyState = state;
}

void RotaryIsr()
{
	static uint32_t lastInterruptTime = 0;
	uint32_t interruptTime = millis();

	if (interruptTime - lastInterruptTime > DebounceDelay && _rotaryState == RotaryIdle)
		_rotaryState = digitalRead(pinRotary) == HIGH ? RotaryUp : RotaryDown;

	lastInterruptTime = interruptTime;
}

void RotaryButtonIsr()
{
	static uint32_t lastInterruptTime = 0;
	uint32_t interruptTime = millis();

	if (digitalRead(pinRotaryButton) == HIGH)
		_rotaryButtonState = (interruptTime - lastInterruptTime) > HoldDelay ? RotaryButtonHold : RotaryButtonPressed;

	lastInterruptTime = interruptTime;
}

void LoadSwitchIsr()
{
	static uint32_t lastInterruptTime = 0;
	uint32_t interruptTime = millis();

	if (interruptTime - lastInterruptTime > DebounceDelay && _loadButtonState == LoadButtonIdle)
		_loadButtonState = digitalRead(pinLoadSwitch) == LOW ? LoadButtonPressed : LoadButtonIdle;
	lastInterruptTime = interruptTime;
}

void TriggerPulseIsr()
{
	static uint32_t lastInterruptTime = 0;
	uint32_t interruptTime = millis();
	if (interruptTime - lastInterruptTime > DebounceDelay && _triggerPulseState == TriggerIdle)
		_triggerPulseState = digitalRead(pinTriggerPulse) == LOW ? TriggerLow : TriggerHigh;
}

//---------------------------------Initial Set up---------------------------------------

void setup()
{
	Serial.begin(9600);                                      //used for testing only

	Wire.begin();                                            //join i2c bus (address optional for master)
	Wire.setClock(400000L);                                  //sets bit rate to 400KHz

	pinMode(pinFan, OUTPUT);
	pinMode(pinTemperature, INPUT);
	pinMode(pinRotaryIrq, INPUT_PULLUP);
	pinMode(pinRotary, INPUT_PULLUP);
	pinMode(pinRotaryButton, INPUT_PULLUP);
	pinMode(pinLoadSwitch, INPUT_PULLUP);
	pinMode(pinTriggerPulse, INPUT_PULLUP);
	TCCR2B = (TCCR2B & 0b11111000) | 1;                      //change PWM to above hearing (Kenneth Larvsen recommendation)

	analogReference(INTERNAL);                               //use Arduino internal reference for temperature monitoring

	ClearInputs();

	_dac.begin(0x61);                                       //the DAC I2C address with MCP4725 pin A0 set high
	_dac.setVoltage(0, true);

	_currentVoltageControl.init();

	enableInterrupt(pinRotaryIrq | PINCHANGEINTERRUPT, RotaryIsr, FALLING);
	enableInterrupt(pinRotaryButton, RotaryButtonIsr, CHANGE);
	enableInterrupt(pinLoadSwitch, LoadSwitchIsr, CHANGE);
	enableInterrupt(pinTriggerPulse, TriggerPulseIsr, CHANGE);
	_keypad.addEventListener(KeypadIsr);

	_lcd.begin(20, 4);                                       //set up the LCD's number of columns and rows 
	_lcd.setBacklightPin(3, POSITIVE);                       // BL, BL_POL
	_lcd.setBacklight(HIGH);                                 //set LCD backlight on
	_lcd.noCursor();                                         //don't show cursor on LCD

	_userData.read();

	if (!_userData.getValue(SkipFlashScreen)) FlashScreenMode::PrintFlashScreen(&_lcd, true);

	ModeSwitch(ModeCC);
}

//------------------------------------------Main Program Loop---------------------------------

void loop()
{
	if (_currentMode && _currentMode->ExitTo != ModeNone) ModeSwitch(_currentMode->ExitTo);
	_keypad.getKeys();
	DclInputs inputs(_keypadKey, _keypadKeyState, _rotaryState, _rotaryButtonState, _loadButtonState, _triggerPulseState);
	ClearInputs();

	if (HandleModeSwitch(&inputs) || !_currentMode) return;

	_currentMode->Update(&inputs);
	if (_currentMode->_userInput) _currentMode->_userInput->HandleInput(&inputs);
}

void ClearInputs()
{
	_keypadKey = NO_KEY;
	_keypadKeyState = IDLE;
	_rotaryState = RotaryIdle;
	_rotaryButtonState = RotaryButtonIdle;
	_loadButtonState = LoadButtonIdle;
	_triggerPulseState = TriggerIdle;
}

bool HandleModeSwitch(DclInputs* inputs)
{
	if (inputs->Keypad_Key == NO_KEY) return false;

	bool hold = inputs->KeyPad_State == HOLD;
	switch (inputs->Keypad_Key)
	{
	case KeyA: return ModeSwitch(hold ? ModeTR : ModeCC);
	case KeyB: return ModeSwitch(ModeCP);
	case KeyC: return ModeSwitch(hold ? ModeSetup : ModeCR);
	case KeyD: return ModeSwitch(hold ? ModeFlash : ModeBC);
	}
	return false;
}

bool ModeSwitch(DclMode mode)
{
	if (mode == ModeNone) return false;

	if (_currentMode) delete _currentMode;
	_currentMode = NULL;

	switch (mode)
	{
		case ModeCP: _currentMode = new ConstantPowerMode(&_lcd, &_dacControl, &_temperatureControl, &_currentVoltageControl, &_userData); break;
		case ModeCR: _currentMode = new ConstantResistanceMode(&_lcd, &_dacControl, &_temperatureControl, &_currentVoltageControl, &_userData); break;
		case ModeBC: _currentMode = new BatteryCapacityMode(&_lcd, &_dacControl, &_temperatureControl, &_currentVoltageControl, &_userData, &_timer); break;
		case ModeTR: _currentMode = new TransientMode(&_lcd, &_dacControl, &_temperatureControl, &_currentVoltageControl, &_userData); break;
		case ModeSetup: _currentMode = new SetupMode(&_lcd, &_userData); break;
		case ModeFlash: _currentMode = new FlashScreenMode(&_lcd, &_userData); break;
		case ModeCC:
		default: _currentMode = new ConstantCurrentMode(&_lcd, &_dacControl, &_temperatureControl, &_currentVoltageControl, &_userData); break;
	}
	
	_currentMode->Initialize();
	return true;
}
