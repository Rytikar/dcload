// TransientLoopController.h

#ifndef _TRANSIENTLOOPCONTROLLER_h
#define _TRANSIENTLOOPCONTROLLER_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include <arduino.h>
#else
	#include "WProgram.h"
#endif

#include <LiquidCrystal_I2C.h>
#include "TransientController.h"
#include "TransientListController.h"
#include "UserData.h"
#include "UserInput.h"
#include "DclConst.h"

class TransientLoopController : public TransientListController
{
public:
	TransientLoopController(LiquidCrystal_I2C* lcd, UserData* userData, UserInput* userInput);
	void Initialize() override;
	void inputCallback(UserInputState state, byte step) override;
};

#endif

