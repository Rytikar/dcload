#ifndef UserData_H
#define UserData_H

#include <Arduino.h>
#include "DclConst.h"

const byte SetupValuesCount = 5;
typedef enum { CurrentCutOff, PowerCutOff, TemperatureCutOff, FanStartupTemperature, SkipFlashScreen } SetupValue;
static const byte SetupMaxValues[SetupValuesCount] = { MaxCurrent, MaxCurrent*MaxVoltage , 99, 99, 1 };
static const byte SetupInputLengths[SetupValuesCount] = { 1, 3, 2, 2, 1 };
static const char SetupUnits[SetupValuesCount] = { 'A', 'W', 'C', 'C', ' ' };

class UserData
{
public:
	UserData(void);
	~UserData(void);
	void read(void);
	void write(void);
	byte getValue(SetupValue setupValue);
	void setValue(SetupValue setupValue, byte value);
	bool isValid(void);
	float getUserBatteryCutOff();
	void writeUserBatteryCutOff(float value);
	static const __FlashStringHelper* getText(SetupValue setupValue);
private:
	const int SetupValuesEEAddress = 0x00;
	const int UserBatteryCutOffEEAddress = 0x100;
	byte* _setupValues;
	float _userBatteryCutOff;
	byte readMinMax(int idx, byte min, byte max, byte def);
	float readMinMax(int address, float min, float max, float def);
	float writeFloat(int address, float value);
};

#endif
