#ifndef DclConst_H
#define DclConst_H

#include <Keypad.h>

typedef enum { UserInputChanged, UserInputConfirmed } UserInputState;
typedef enum { RotaryIdle, RotaryDown, RotaryUp } RotaryState;
typedef enum { RotaryButtonIdle, RotaryButtonPressed, RotaryButtonHold } RotaryButtonState;
typedef enum { LoadButtonIdle, LoadButtonPressed } LoadButtonState;
typedef enum { TemperatureStateFanOff, TemperatureStateFanOn, TemperatureStateCutOff } TemperatureState;
typedef enum { TriggerIdle, TriggerLow, TriggerHigh } TriggerPulseState;

typedef int Temperature;

struct CurrentVoltage
{
	float current;
	float voltage;

	CurrentVoltage::CurrentVoltage(float current, float voltage)
	{
		this->current = current;
		this->voltage = voltage;
	}
};

struct CurrentTemperature
{
	Temperature temperature;
	TemperatureState temperatureState;

	CurrentTemperature::CurrentTemperature(Temperature temperature, TemperatureState temperatureState)
	{
		this->temperature = temperature;
		this->temperatureState = temperatureState;
	}
};

struct DclInputs
{
	KeypadEvent Keypad_Key;
	KeyState KeyPad_State;
	RotaryState Rotary_State;
	RotaryButtonState RotaryButton_State;
	LoadButtonState LoadButton_State;
	TriggerPulseState TriggerPulse_State;

	DclInputs::DclInputs(KeypadEvent key, KeyState keyState, RotaryState rotaryState, RotaryButtonState rotaryButtonState, LoadButtonState loadButtonState, TriggerPulseState triggerPulseState)
	{
		Keypad_Key = key;
		KeyPad_State = keyState;
		Rotary_State = rotaryState;
		RotaryButton_State = rotaryButtonState;
		LoadButton_State = loadButtonState;
		TriggerPulse_State = triggerPulseState;
	}
};

const char NulChar = '\0';
const char DecimalPointChar = '.';
const char CommaChar = ',';
const char SpaceChar = ' ';
const char FanOnChar = 'F';

const char CurrentQuantity = 'I';
const char VoltageQuantity = 'U';
const char PowerQuantity = 'W';
const char ResistanceQuantity = 'R';

const char CurrentUnit = 'A';
const char VoltageUnit = 'V';
const char PowerUnit = 'W';
const char ResistanceUnit = (char)0xF4;
const char TemperatureUnit = 'C'; // (char)0xDF)
const char SecondUnit = 'S';

const char KeyHash = '#';
const char KeyStar = '*';
const char KeyA = 'A';
const char KeyB = 'B';
const char KeyC = 'C';
const char KeyD = 'D';

const byte MaxCurrent = 5;
const byte MaxVoltage = 50;

const byte LcdRows = 4;
const byte LcdColumns = 20;

const byte KEYPADROWS = 4;
const byte KEYPADCOLS = 4;

const byte pinRotaryIrq = 2;                  //digital pin (also interrupt pin) for the A pin of the Rotary Encoder (changed to digital pin 2)
const byte pinFan = 3;                        //digital pin 3 for fan control output (changed to Digital pin 3)
const byte pinRotary = 4;                     //digital pin for the B pin of the Rotary Encoder
const byte pinRotaryButton = 17;              //analog pin A3 used as a digital pin to set cursor position (rotary encoder push button)
const byte pinLoadSwitch = 15;                //analog pin A1 used as a digital pin to set Load ON/OFF
const byte pinTriggerPulse = 16;              //analog pin A2 used as a digital pin for trigger pulse input in transient mode
const byte pinTemperature = 20;               //analog pin used for temperature output from LM35 (was A0 previously but changed)

const byte StatusRow = 0;
const byte ActualRow = 1;
const byte ModeRow = 2;
const byte ExtraRow = 3;

const int ConvenienceDelay = 2000;
const int DebounceDelay = 100;
const int HoldDelay = 1000;

#endif
