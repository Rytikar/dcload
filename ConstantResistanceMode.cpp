#include <Arduino.h>
#include <LiquidCrystal_I2C.h>
#include "ConstantResistanceMode.h"
#include "PowerMode.h"
#include "ManualPowerMode.h"
#include "UserData.h"

ConstantResistanceMode::ConstantResistanceMode(
  LiquidCrystal_I2C* lcd, 
  DacControl* dacControl,
  TemperatureControl* temperatureControl, 
  CurrentVoltageControl* currentVoltageControl, 
  UserData* userData) : ManualPowerMode(lcd, dacControl, temperatureControl, currentVoltageControl, userData){}
DclMode ConstantResistanceMode::GetMode(){ return ModeCR; }
char ConstantResistanceMode::Quantity(){ return ResistanceQuantity; }
void ConstantResistanceMode::UserInputSetup() { _userInput->Setup(0, 0, 99999, UserInputLength, 0, this, UserInputColumn, ModeRow, ResistanceUnit); }

float ConstantResistanceMode::GetDacCurrent()
{
  float resistance = _userInput->ToFloat();
  return resistance <= 0 ? 0 : _currentVoltage.voltage / resistance;
}

