#ifndef ConstantCurrentMode_H
#define ConstantCurrentMode_H
 
#include <Arduino.h>
#include <LiquidCrystal_I2C.h>
#include "PowerMode.h"
#include "ManualPowerMode.h"
#include "UserData.h"

class ConstantCurrentMode : public ManualPowerMode
{
  public:
    ConstantCurrentMode(
      LiquidCrystal_I2C* lcd, 
	  DacControl* dacControl,
      TemperatureControl* temperatureControl, 
      CurrentVoltageControl* currentVoltageControl, 
      UserData* userData);
    char Quantity() override;
    void UserInputSetup() override;
    DclMode GetMode() override;
  protected:
    float GetDacCurrent() override;
};
 
#endif

