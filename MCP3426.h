#ifndef MCP3426_H
#define MCP3426_H

#include <Arduino.h>

class MCP3426 {
public:
	static const byte CHANNEL_0 = 0;
	static const byte CHANNEL_1 = 1;

	static const byte CONFIG_C0 = B10011000; // channel0, constant conversion, 16bit, gain 1
	static const byte SELECT_C0 = B00011000;
	static const byte CONFIG_C1 = B10111010; // channel1, constant conversion, 16bit, gain 4
	static const byte SELECT_C1 = B00111010;
	double VRef;

	void init();
	int readADC(long* value);
private:
	byte _i2cAddress;
	byte _currentChannel;
};
#endif