#ifndef FlashScreenMode_H
#define FlashScreenMode_H
 
#include <Arduino.h>
#include <LiquidCrystal_I2C.h>
#include "UserData.h"
#include "UserInput.h"
#include "BaseMode.h"

class FlashScreenMode : public BaseMode
{
  public:
    FlashScreenMode(LiquidCrystal_I2C* lcd, UserData* userData);
	void Initialize() override;
	bool UpdateUI() override;
	void inputCallback(UserInputState state, byte step) override;
	DclMode GetMode();
    static void PrintFlashScreen(LiquidCrystal_I2C* lcd, bool doDelay);
    static int FreeRam();
  private:
};
 
#endif
