#include <Arduino.h>
#include <LiquidCrystal_I2C.h>
#include "BaseMode.h"
#include "UserData.h"
#include "UserInput.h"

BaseMode::BaseMode(LiquidCrystal_I2C* lcd, UserData* userData)
{
	_lcd = lcd;
	_userData = userData;
}

BaseMode::~BaseMode()
{
	if (_userInput) delete _userInput;
	_userInput = NULL;
}

void BaseMode::Update(DclInputs* inputs)
{
	_currentMillis = millis();
	if (_nextMillis > _currentMillis) return;
	_nextMillis = _currentMillis + 100;
	TimerElapsed100ms();
}

void BaseMode::TimerElapsed100ms()
{
	_100msToggle = !_100msToggle;
	_current100ms++;
	if (UpdateUI()) printInput();
	if (_current100ms < 10) return;
	TimerElapsed1s();
	_current100ms = 0;
}

void BaseMode::TimerElapsed1s()
{
	_1sToggle = !_1sToggle;
	_seconds++;
}

//---------------------

bool BaseMode::UpdateUI()
{
	static const char* Names[7] = { "CC", "CP", "CR", "BC", "TR", "SU", "FL" };
	_lcd->setCursor(ModeNameColumn, StatusRow);
	_lcd->print(Names[GetMode() - 1]);
	return false;
}

void BaseMode::printInput()
{
	int cursorCol = _userInput->GetCursorCol();
	if (cursorCol < 0) _lcd->noCursor();
	else
	{
		_lcd->cursor();
		_lcd->setCursor(_userInput->_column, _userInput->_row);
		_lcd->print(_userInput->ToString());
		if (_userInput->_unit) _lcd->print(_userInput->_unit);
		_lcd->setCursor(cursorCol, _userInput->_row);
	}
}
