#ifndef BatteryCapacityMode_H
#define BatteryCapacityMode_H
 
#include <Arduino.h>
#include <LiquidCrystal_I2C.h>
#include <MCP79410_Timer.h>
#include "PowerMode.h"
#include "ManualPowerMode.h"
#include "UserData.h"

class BatteryCapacityMode : public ManualPowerMode
{
  public:
    BatteryCapacityMode(
      LiquidCrystal_I2C* lcd, 
 	  DacControl* dacControl,
      TemperatureControl* temperatureControl, 
      CurrentVoltageControl* currentVoltageControl, 
      UserData* userData,
      MCP79410_Timer* timer);
    char Quantity() override;
    void UserInputSetup() override;
    DclMode GetMode() override;
    void Update(DclInputs* inputs) override;
	bool UpdateUI() override;
	void Initialize() override;
	void inputCallback(UserInputState state, byte step);
protected:
    float GetDacCurrent();
    virtual void LoadSwitched(bool on) override;
  private:
    const byte BatteryTypeCount = 6;
    const byte BatteryCutOffColumn = 15;
    const int BatteryLifetimeSteps = 1000;
    MCP79410_Timer* _timer;
    byte _step = 0;
    byte _selectedType = 0;
    float _selectedCutOff = 0;
    unsigned long _batteryLifeMillis = 0;
    float _batteryLifeAccumulated = 0;
    void HandleType(int selectedType);
    void HandleTimer(CurrentVoltage* currentVoltage);
    void FinalSetup(float selectedCutOff);
};
 
#endif

