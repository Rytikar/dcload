// ManualPowerMode.h

#ifndef _MANUALPOWERMODE_h
#define _MANUALPOWERMODE_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include <arduino.h>
#else
	#include "WProgram.h"
#endif

#include "PowerMode.h"

class ManualPowerMode : public PowerMode
{
public:
	ManualPowerMode(LiquidCrystal_I2C* lcd, DacControl* dacControl, TemperatureControl* temperatureControl, CurrentVoltageControl* currentVoltageControl, UserData* userData);
	bool UpdateUI() override;
	void Initialize() override;
	void inputCallback(UserInputState state, byte step);
	virtual char Quantity() = 0;
	virtual void UserInputSetup() = 0;
protected:
	const byte UserInputLength = 5;
	const byte UserInputColumn = 6;
	const byte UnitColumn = 13;
};

#endif

