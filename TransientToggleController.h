// TransientToggleController.h

#ifndef _TRANSIENTTOGGLECONTROLLER_h
#define _TRANSIENTTOGGLECONTROLLER_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include <arduino.h>
#else
	#include "WProgram.h"
#endif

#include <LiquidCrystal_I2C.h>
#include "TransientController.h"
#include "UserData.h"
#include "UserInput.h"
#include "DclConst.h"

class TransientToggleController : public TransientController
{
public:
	TransientToggleController(LiquidCrystal_I2C* lcd, UserData* userData, UserInput* userInput);
	void Initialize() override;
	bool Update(DclInputs* inputs) override;
	void UpdateUI() override;
	void SwitchLoad(bool on) override;
	void inputCallback(UserInputState state, byte step) override;
private:
	float _current1;
	float _current2;
};


#endif

