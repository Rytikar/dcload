#include <Arduino.h>
#include <Keypad.h>
#include <LiquidCrystal_I2C.h>
#include "DclConst.h"
#include "UserInput.h"

char _keys[MaxInputLength + 1];

byte UserInput::GetInputLength() { return _inputLength; }
float UserInput::ToFloat() { return DclTools::str2Float(_keys); }
int UserInput::ToInt() { return (int)ToFloat(); }
long UserInput::ToLong() { return (long)ToFloat(); }
const char* UserInput::ToString() { return _keys; }
bool UserInput::IsConfirmInput(DclInputs* inputs){ 	return ((inputs->KeyPad_State == HOLD && inputs->Keypad_Key == KeyStar) || (inputs->RotaryButton_State == RotaryButtonHold)); }


byte UserInput::Setup(byte minValue, byte maxValue, InputCallback* inputCallback, byte step = 0)
{
	return setup(0, minValue, maxValue, 1, 0, inputCallback, true, step);
}

byte UserInput::Setup(float value, float minValue, float maxValue, byte inputLength, byte precision, InputCallback* inputCallback, byte column = 0, byte row = 0, char unit = NulChar, byte step = 0)
{
	_column = column;
	_row = row;
	_unit = unit;
	return setup(value, minValue, maxValue, inputLength, precision, inputCallback, false, step);
}

byte UserInput::setup(float value, float minValue, float maxValue, byte inputLength, byte precision, InputCallback* inputCallback, bool singleKey, byte step)
{
	_minValue = minValue;
	_maxValue = maxValue;
	_precision = precision;
	_inputLength = inputLength;
	_inputIntLength = _precision == 0 ? _inputLength : _inputLength - _precision - 1;
	_singleKey = singleKey;
	_step = step;
	_inputCallback = inputCallback;
	clearKeys(value < minValue || value > maxValue ? minValue : value);
	return step;
}

void UserInput::HandleInput(DclInputs* inputs)
{
	if (_singleKey)
	{
		if (HandleSingleKey(inputs->Keypad_Key, inputs->KeyPad_State)) Callback(UserInputConfirmed);
	}
	else
	{
		if (IsConfirmInput(inputs))
			Callback(UserInputConfirmed);
		else if (HandleKey(inputs->Keypad_Key, inputs->KeyPad_State) || HandleRotary(inputs->Rotary_State) || HandleRotaryButton(inputs->RotaryButton_State))
			Callback(UserInputChanged);
	}
}

bool UserInput::HandleSingleKey(char key, KeyState keyState)
{
	if ((keyState != RELEASED) || (key < ('0' + (byte)_minValue)) || (key > ('0' + (byte)_maxValue))) return false;
	_keys[0] = key;
	_keys[1] = NulChar;
	return true;
}

bool UserInput::HandleKey(char key, KeyState keyState)
{
	switch (keyState)
	{
	case RELEASED: return HandleReleaseKey(key);
	case HOLD: return HandleHoldKey(key);
	default: return false;
	}
}

bool UserInput::HandleReleaseKey(char key)
{
	switch (key)
	{
	case KeyStar: return toggleInFrac();
	case KeyHash: return deleteKey();
	default: return addKey(key, true);
	}
}

bool UserInput::HandleHoldKey(char key)
{
	if (key == KeyHash) return clearKeys(_minValue);
	return false;
}

bool UserInput::setKeys(const char* keys, int cursor)
{
	float f = DclTools::str2Float(keys);
	if (f < _minValue || f > _maxValue) return false;
	DclTools::floatToStr(f, _inputLength, _precision, _keys);
	return setCursor(cursor);
}

bool UserInput::setCursor(int cursor)
{
	if (cursor < 0) cursor = 0;
	else if (_inFrac && cursor >= _precision) cursor = _precision - 1;
	else if (!_inFrac && cursor >= _inputIntLength - 1) cursor = _inputIntLength - 1;
	_cursor = cursor;
	return true;
}

bool UserInput::toggleInFrac(byte cursor = 0)
{
	if (_precision == 0) return false;
	_inFrac = !_inFrac;
	return setCursor(cursor);
}

bool UserInput::addKey(char key, bool advanceCursor)
{
	char keys[MaxInputLength+1];
	strcpy(keys, _keys);
	if (_inFrac)
	{
		keys[_cursor + (_inputLength - _precision)] = key;
		return setKeys(keys, _cursor + 1);
	}
	else
	{
		for (byte i = 1; i < _inputIntLength - _cursor; i++) keys[i-1] = keys[i];
		keys[(_inputIntLength - _cursor) - 1] = key;
		return setKeys(keys, _cursor);
	}
}

bool UserInput::deleteKey()
{
	char keys[MaxInputLength + 1];
	DclTools::floatToStr(ToFloat(), _inputLength, _precision, keys, '0');
	if (_inFrac)
	{
		for (int i = _cursor + (_inputLength - _precision); i < _inputLength - 1; i++) keys[i] = keys[i + 1];
		keys[_inputLength - 1] = '0';
		return setKeys(keys, _cursor - 1);
	}
	else
	{
		for (int i = (_inputIntLength - _cursor) - 1; i > 0; i--) keys[i] = keys[i - 1];
		keys[0] = '0';
		return setKeys(keys, _cursor);
	}
}

bool UserInput::clearKeys(float value)
{
	_inFrac = false;
	_cursor = 0;
	DclTools::floatToStr(value, _inputLength, _precision, _keys);
	Callback(UserInputChanged);
	return true;
}

bool UserInput::HandleRotary(RotaryState rotaryState)
{
	char keys[MaxInputLength + 1];

	if (rotaryState == RotaryIdle) return false;

	bool down = rotaryState == RotaryDown;
	DclTools::floatToStr(ToFloat(), _inputLength, _precision, keys, '0');
	byte pos = _inFrac ? _inputIntLength + 1 + _cursor : _inputIntLength - 1 - _cursor;
	if (down && keys[pos] > '0') keys[pos]--;
	else if (!down && keys[pos] < '9') keys[pos]++;
	return setKeys(keys, _cursor);
}

bool UserInput::HandleRotaryButton(RotaryButtonState rotaryButtonState)
{
	if (rotaryButtonState != RotaryButtonPressed) return false;

	if (_inFrac)
		return (_cursor < _precision - 1) ? setCursor(_cursor + 1) : toggleInFrac(_inputIntLength - 1);
	else
		return (_cursor > 0) ? setCursor(_cursor - 1) : (_precision == 0 ? setCursor(_inputIntLength - 1) : toggleInFrac(0));
}

void UserInput::Callback(UserInputState state)
{
	_inputCallback->inputCallback(state, _step);
}

int UserInput::GetCursorCol()
{
	if (_singleKey) return -1;
	if (_precision == 0) return _column + ((_inputLength - _cursor) - 1);
	else
	{
		byte decimalPointPosition = (_inputLength - _precision) - 1;
		byte col = _inFrac ? decimalPointPosition + (_cursor + 1) : decimalPointPosition - (_cursor + 1);
		return _column + col;
	}
}
