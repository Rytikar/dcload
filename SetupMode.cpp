#include <Arduino.h>
#include "SetupMode.h"
#include "DclConst.h"
#include "DclTools.h"
#include "UserData.h"
#include "BaseMode.h"

SetupMode::SetupMode(LiquidCrystal_I2C* lcd, UserData* userData) : BaseMode(lcd, userData)
{}

void SetupMode::Initialize()
{
	_userInput = new UserInput();
	PrintCurrentSetup(_lcd, _userData);
	Reset();
	InitializeStep();
}

bool SetupMode::UpdateUI() 
{
	return true;
}

void SetupMode::inputCallback(UserInputState state, byte step)
{
	if (state == UserInputConfirmed) NextStep();
}

SetupPosition SetupMode::GetSetupPosition(byte item, byte itemCount)
{
  byte row = item%ValueRows;
  return SetupPosition((byte)(item/ValueRows), row, row == 0, row == ValueRows-1 || item == itemCount-1);
}

void SetupMode::PrintCurrentSetup(LiquidCrystal_I2C* lcd, UserData* userData)
{
	for (int i = 0; i < SetupValuesCount; i++)
	{
		SetupPosition setupPosition = GetSetupPosition(i, SetupValuesCount);
		if (setupPosition.PageSkip) PrintSetupHeader(lcd, "Current", setupPosition.Page);
		PrintSetup(lcd, setupPosition.Row + ValueFirstRow, i, userData->getValue((SetupValue)i));
		if (setupPosition.LastItem) delay(ConvenienceDelay);
	}
}

void SetupMode::PrintSetupHeader(LiquidCrystal_I2C* lcd, const char* text, int page)
{
	lcd->clear();
	lcd->setCursor(1, StatusRow);
	lcd->print(text);
	lcd->print(F(" Setup ("));
	lcd->print(page + 1);
	lcd->print("/");
	lcd->print((SetupValuesCount / ValueRows) + 1);
	lcd->print(")");
}

void SetupMode::PrintSetup(LiquidCrystal_I2C* lcd, byte row, byte item, int value)
{
  lcd->setCursor(0, row);
  lcd->print(UserData::getText((SetupValue)item));
  lcd->setCursor(13, row);
  lcd->print("=");
  DclTools::PrintValue(lcd, ValueColumn, row, value, SetupInputLengths[item], 0, SetupUnits[item]);
}

void SetupMode::Reset()
{
  _lcd->clear();
  _step = 0;
}

void SetupMode::InitializeStep()
{
  SetupPosition setupPosition = GetSetupPosition(_step, SetupValuesCount);
  if (setupPosition.PageSkip) PrintSetupHeader(_lcd, "Enter", setupPosition.Page);
  PrintSetup(_lcd, ValueFirstRow + setupPosition.Row, _step, 0);
  byte value = _userData->getValue((SetupValue)_step);
  byte maxValue = SetupMaxValues[_step];
  _userInput->Setup(_userData->getValue((SetupValue)_step), 0, SetupMaxValues[_step], SetupInputLengths[_step], 0, this, ValueColumn, (_step%ValueRows) + ValueFirstRow, SetupUnits[_step], _step);
}

void SetupMode::NextStep()
{
  _userData->setValue((SetupValue)_step, _userInput->ToInt());
  
  if (_step == SetupValuesCount - 1)
  {
    if (_userData->isValid()) 
    {
      _userData->write();
      ExitTo = ModeCC;
    }
    else Reset();//TODO make error more clear
  }
  else
    _step++;

  InitializeStep();
}

DclMode SetupMode::GetMode(){ return ModeSetup; }

