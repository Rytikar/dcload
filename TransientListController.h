// TransientListController.h

#ifndef _TRANSIENTLISTCONTROLLER_h
#define _TRANSIENTLISTCONTROLLER_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include <arduino.h>
#else
	#include "WProgram.h"
#endif

#include <LiquidCrystal_I2C.h>
#include "TransientController.h"
#include "UserData.h"
#include "UserInput.h"
#include "DclConst.h"

struct TransientListItem
{
	float current;
	long time;
};

class TransientListController : public TransientController
{
public:
	TransientListController(LiquidCrystal_I2C* lcd, UserData* userData, UserInput* userInput);
	virtual ~TransientListController();
	void Initialize() override;
	bool Update(DclInputs* inputs) override;
	void UpdateUI() override;
	void SwitchLoad(bool on) override;
	void inputCallback(UserInputState state, byte step) override;
protected:
	TransientListItem * _listItems = NULL;
	byte _itemCount = 0;
	int _currentItem = 0;
	byte _nextItem = 0;
	long _nextMillis;
	long _leftMillis;
	void setupItem(byte item);
	void TransientListController::finishSetup();
};


#endif

